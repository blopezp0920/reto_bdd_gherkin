# language: es


Característica: Operaciones de cuenta de ahorro
  Como un cliente del banco que posee una cuenta de ahorro en moneda nacional
  Necesito operar mi cuenta de ahorro
  Para gestionar mi dinero.

  Antecedentes:
  Dado que el cliente está logeado y se encuentra en su panel de inicio o usuario.

  Escenario: transferir dinero a otra cuenta propia o de terceros
    Cuando suministre los datos solicitados para la trasferencia y los envía
    Entonces se muestra un mensaje con los datos de la transferencia a la espera de la confirmación.

  Escenario: verifica y confirma los datos de la transferencia
    Dado que el cliente suministro los datos de la transferencia
      Y los envió, estando en la pantalla de confirmación
    Cuando el cliente verificó los datos y envía la confirmación
    Entonces se muestra el mensaje que la transferencia fue exitosa.

  Escenario: consultar el saldo de su cuenta
    Cuando seleccione la opción de consultar el saldo
    Entonces se mostrará el saldo disponible del cliente.

  Escenario: consultar últimos movimientos
    Cuando seleccione la opción de ver sus últimos movientos de la cuenta
    Entonces en pantalla se mostrará los últimos quince movimientos de la cuenta.

  Escenario: consultar pago de servicios
    Cuando seleccione la opción pago de servicios
    Entonces se mostrará en pantalla los servicios pendientes de pago.

  Escenario: pagar un servicio
    Dado que el cliente se encuentra en la pantalla de servicios pendientes de pago
    Cuando seleccione uno de los servicios del listado
      Y el cliente posea saldo suficiente
    Entonces se mostrará en pantalla que el pago se realizó satisfactoriamente.

  Escenario: generar cobro por qr
    Cuando se seleccione la opción de generar cobro por QR
    Entonces se mostrará en pantalla el código QR que posee la información de pago.

  Escenario: consultar los datos de su cuenta (n° de cuenta, cbu, alias)
    Cuando seleccione la opción de consultar los datos de su cuenta de ahorro
    Entonces se le mostrará en pantalla sus datos persoles y de cuenta de ahorro.